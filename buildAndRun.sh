#!/bin/sh
mvn clean package && docker build -t com.ark/gcloud-app .
docker rm -f gcloud-app || true && docker run -d -p 8080:8080 -p 4848:4848 --name gcloud-app com.ark/gcloud-app 
